package com.example.neopez.propiedades;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.neopez.CardNeopez;
import com.example.neopez.R;
import com.example.neopez.VerPropiedades;
import com.example.neopez.persistencia.DbEstanque;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;


public class CrearPropiedades extends Fragment {

    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = firebaseFirestore.getInstance();
    AdaptadorPropiedades adaptadorPropiedades;
    RecyclerView recyclerView;

    public CrearPropiedades() {

    }


    public static CrearPropiedades newInstance() {
        CrearPropiedades fragment = new CrearPropiedades();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       View root = inflater.inflate(R.layout.fragment_crear_propiedades, container, false);

        TextInputEditText tfEstanque = root.findViewById(R.id.tfEstanque);
        TextInputEditText tfUbicacion = root.findViewById(R.id.tfUbicacion);
        TextInputEditText tftipodepez = root.findViewById(R.id.tftipodepez);
        Button btnAgregar = root.findViewById(R.id.btnAgregar);


        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(tfEstanque.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tfUbicacion.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tftipodepez.getText().toString().trim())) {

                    DbEstanque db = new DbEstanque(root.getContext());
                    db.agregarPropiedades(tfEstanque.getText().toString().trim(), tfUbicacion.getText().toString().trim(), tftipodepez.getText().toString().trim());
                    Toast.makeText(getActivity(), "Creada la propiedad: " + tfEstanque.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                    ((CardNeopez) getActivity()).switchWindow(new VerPropiedades());
                    db.close();

                } else {
                    Toast.makeText(root.getContext(), "Error, hay campos sin llenar", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return root;
    }
}


