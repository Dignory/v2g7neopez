package com.example.neopez.propiedades;


import java.util.Random;

public class ListPropiedades {

    private String nombre;
    private int codigoestanque;
    private String ubicacion;
    private String tipodepez;

    public ListPropiedades(String nombre, int codigoestanque, String ubicacion, String tipodepez) {
        this.nombre = nombre;
        this.codigoestanque = codigoestanque;
        this.ubicacion = ubicacion;
        this.tipodepez = tipodepez;
    }

    public ListPropiedades(){
        Random rand = new Random();
        this.nombre = "Item" + rand.nextInt();
        this.codigoestanque =  rand.nextInt();
        this.ubicacion = "" +rand.nextInt();
        this.tipodepez = rand.toString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigoestanque() {
        return codigoestanque;
    }

    public void setCodigoestanque(int codigoestanque) {
        this.codigoestanque = codigoestanque;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public float getTipodepez() {
        return tipodepez;
    }

    public void setTipodepez(String tipodepez) {
        this.tipodepez = tipodepez;
    }
}