package com.example.neopez.propiedades;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.neopez.CardNeopez;
import com.example.neopez.R;
import com.example.neopez.VerPropiedades;
import com.example.neopez.persistencia.DbEstanque;


import java.util.ArrayList;
import java.util.List;

public class PropiedadesAdapter extends RecyclerView.Adapter<PropiedadesAdapter.PropiedadesViewHolder> {
    List<ListPropiedades> listapropiedades = new ArrayList<>();

    @NonNull
    @Override
    public PropiedadesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_propiedades, null, false);
        return new PropiedadesViewHolder(view, view.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull PropiedadesViewHolder holder, int position) {
        holder.asignarPropiedades(listapropiedades.get(position));
    }

    public void añadirPropiedades(ListPropiedades lp){
        ListPropiedades.add(lp);
        notifyDataSetChanged();
    }

    public void eliminarPropiedades(ListPropiedades lp, Context context){
        DbEstanque c = new  DbEstanque(context);
        c.eliminarPropiedades(Integer.toString(lp.getCodigoestanque()));
        Toast.makeText(context, "Se ha eliminado: " + lp.getCodigoestanque(), Toast.LENGTH_SHORT).show();
        c.close();
        listapropiedades.remove(lp);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return listapropiedades.size();
    }

    class PropiedadesViewHolder extends RecyclerView.ViewHolder {

        TextView idestanque;
        TextView estanquenom;
        TextView ubicacion;
        TextView tipodepez;
        Button btnEdit;
        Button btnRemove;
        Context context;

        public PropiedadesViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            idestanque = itemView.findViewById(R.id.codigoestanque);
            estanquenom = itemView.findViewById(R.id.nombre);
            ubicacion = itemView.findViewById(R.id.ubicacion);
            tipodepez = itemView.findViewById(R.id.tipodepez);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnRemove = itemView.findViewById(R.id.btnRemove);
            this.context = context;
        }

        public void asignarPropiedades(ListPropiedades lp) {
            idestanque.setText(Integer.toString(lp.getCodigoestanque()).trim());
            estanquenom.setText(lp.getNombre().trim());
            ubicacion.setText(lp.getUbicacion().trim());
            tipodepez.setText(Float.toString(lp.getTipodepez()).trim());
            btnEdit.setOnClickListener(view -> ((CardNeopez) context).switchWindow(new ActualizarPropiedades(lp)));
            btnRemove.setOnClickListener(view -> {
                DbEstanque c = new DbEstanque(context);
                c.eliminarPropiedades(Integer.toString(lp.getCodigoestanque()));
                Toast.makeText(context, "Se ha eliminado: " +lp.getCodigoestanque(), Toast.LENGTH_SHORT).show();
                c.close();
                ((CardNeopez) context).switchWindow(new VerPropiedades());
            });
        }
    }
}
