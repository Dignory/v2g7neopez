package com.example.neopez.propiedades;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.neopez.CardNeopez;
import com.example.neopez.R;
import com.example.neopez.VerPropiedades;
import com.example.neopez.persistencia.DbEstanque;
import com.google.android.material.textfield.TextInputEditText;


public class ActualizarPropiedades extends Fragment {

    ListPropiedades lp;

    public ActualizarPropiedades(){

    }

    public ActualizarPropiedades(ListPropiedades lp) {
        this.lp = lp;
    }



    public static ActualizarPropiedades newInstance() {
        ActualizarPropiedades fragment = new ActualizarPropiedades();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_actualizar_propiedades, container, false);

        TextInputEditText tfEstanque = root.findViewById(R.id.tfEstanque);
        TextInputEditText tfUbicacion = root.findViewById(R.id.tfUbicacion);
        TextInputEditText tftipodepez = root.findViewById(R.id.tftipodepez);
        Button btnActualizar = root.findViewById(R.id.btnActualizar);

        tfEstanque.setText(lp.getNombre());
        tfEstanque.setEnabled(false);
        tfUbicacion.setText(lp.getUbicacion());
        tftipodepez.setText(Float.toString(lp.getTipodepez()));

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(tfEstanque.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tfUbicacion.getText().toString().trim()) &&
                        !TextUtils.isEmpty(tftipodepez.getText().toString().trim())){


                    DbEstanque db = new DbEstanque(root.getContext());
                    db.ActualizarPropiedades(Integer.toString(lp.getCodigoestanque()).trim(), tfUbicacion.getText().toString().trim(), tftipodepez.getText().toString().trim());
                    Toast.makeText(getActivity(),"Actualizado: "+lp.getNombre(),Toast.LENGTH_SHORT).show();
                    ((CardNeopez) getActivity()).switchWindow(new VerPropiedades());
                    db.close();

                } else {
                    Toast.makeText(root.getContext(), "Error, hay campos sin llenar", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return root;
    }
}





