package com.example.neopez.propiedades;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.neopez.CardNeopez;
import com.example.neopez.R;
import com.example.neopez.VerPropiedades;
import com.example.neopez.persistencia.DbEstanque;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.util.List;

public class AdaptadorPropiedades extends FirestoreRecyclerAdapter<ListPropiedades, AdaptadorPropiedades.ViewHolderPropiedades>{

    List<ListPropiedades> listPropiedades;
    FragmentActivity context;

    public AdaptadorPropiedades(FirestoreRecyclerOptions<ListPropiedades> options){
        super(options);

    }
    @NonNull
    @Override
    public class ViewHolderPropiedades(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_propiedades, null, false);
        return new ViewHolderPropiedades(view, context);

    }


    @Override
    protected void onBindViewHolder(@NonNull ViewHolderPropiedades holder, int position, @NonNull ListPropiedades model){

        holder.idestanque.setText(model.getCodigoestanque());
        holder.estanquenom .setText(model.getNombre());
        holder.ubicacion .setText(model.getUbicacion());
        holder.tipodepez .setText(model.getTipodepez());
    }
        @Override
        public int getItemCount() {
            int size = 0;
            if(listPropiedades != null){
                size = listPropiedades.size();
            }
            return size;
        }


       public class ViewHolderPropiedades extends RecyclerView.ViewHolder {

            TextView idestanque;
            TextView estanquenom;
            TextView ubicacion;
            TextView tipodepez;
            Button btnEdit;
            Button btnRemove;
            FragmentActivity context;


            public ViewHolderPropiedades(@NonNull View itemView, FragmentActivity context) {
                super(itemView);
                idestanque = itemView.findViewById(R.id.codigoestanque);
                estanquenom = itemView.findViewById(R.id.nombre);
                ubicacion = itemView.findViewById(R.id.ubicacion);
                tipodepez = itemView.findViewById(R.id.tipodepez);
                btnEdit = itemView.findViewById(R.id.btnEdit);
                btnRemove = itemView.findViewById(R.id.btnRemove);
                this.context = context;
            }

            public void asignarPropiedades(ListPropiedades lp) {
                idestanque.setText(Integer.toString(lp.getCodigoestanque()).trim());
                estanquenom.setText(lp.getNombre().trim());
                ubicacion.setText(lp.getUbicacion().trim());
                tipodepez.setText(Float.toString(lp.getTipodepez()).trim());
                    DbEstanque c = new DbEstanque(context);
                    c.eliminarPropiedades(Integer.toString(lp.getCodigoestanque()));
                    Toast.makeText(context, "Se ha eliminado: " + lp.getNombre(), Toast.LENGTH_SHORT).show();
                    c.close();
                    ((CardNeopez) context).switchWindow(new VerPropiedades());
                });
            }
        }
    }
