package com.example.neopez;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MenuPrueba extends AppCompatActivity {

    TextView tv1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_prueba);

        tv1 = findViewById(R.id.tv1);

         }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menutemperatura, menu);
            return true;

        }

        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            int nro = item.getItemId();
            float valor;

            //SET = Modificar
            //GET = Obtener

            switch (nro){
                case R.id.estadisticas:
                    valor = tv1.getTextSize();
                    valor = valor + 20;
                    tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                    return true;
                case R.id.graficos:
                    valor = tv1.getTextSize();
                    valor = valor - 30;
                    tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                    return true;
                case R.id.salir:
                    finish();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }
