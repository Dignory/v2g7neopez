package com.example.neopez;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;

public class Snackbarneopez extends AppCompatActivity {
    Button bdefacult, baction, bcustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbarneopez);

        bdefacult = findViewById(R.id.button);
        baction = findViewById(R.id.button2);
        bcustom = findViewById(R.id.button3);

        bdefacult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(view,"Tipo de pez", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
        baction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(view,"El parámetro ha sido eliminado", Snackbar.LENGTH_LONG).setAction("Deshacer", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar = Snackbar.make(view, "El parámetro ha sido recuperado", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
                snackbar.show();
            }
        });

        bcustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(view,"Elija estanque", Snackbar.LENGTH_LONG).setAction("Deshacer", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
    }

}

