package com.example.neopez.persistencia;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class NeofishDb extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=4;
    private static final String DATABASE_NOMBRE = "neofish.db";
    public static final String TABLE_USERS = "usuarios";
    public static final String TABLE_TIPOPEZ = "tipopez";
    public static final String TABLE_MEDICIONES = "mediciones";
    public static final String TABLE_ALBERCA = "alberca";
    public static final String TABLE_ENCENDERAIREADOR = "encenderaireador";

    public NeofishDb(@Nullable Context context) {
        super (context, DATABASE_NOMBRE,null, DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_USERS + "("+
                "idusuario INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nomusuario TEXT NOT NULL,"+
                "contrasena TEXT NOT NULL," +
                "correo TEXT NOT NULL)");

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_TIPOPEZ + "("+
                "idtipopez INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombre STRING NOT NULL, " +
                "descripcion STRING NOT NULL,"+
                "temperatura_max DOUBLE NOT NULL, " +
                "temperatura_min DOUBLE NOT NULL,"+
                "oxigeno_max DOUBLE NOT NULL, " +
                "oxigeno_min DOUBLE NOT NULL,"+
                "ph_max INTEGER NOT NULL, " +
                "ph_min INTEGER NOT NULL)");

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_MEDICIONES + "("+
                "idmedicion INTEGER PRIMARY KEY AUTOINCREMENT," +
                "idestanque INTEGER NOT NULL, " +
                "fechamedicion DATETIME NOT NULL,"+
                "oxigeno DOUBLE NOT NULL,"+
                "pH DOUBLE NOT NULL,"+
                "temperatura DOUBLE NOT NULL)");

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_ALBERCA + "("+
                "idestanque INTEGER PRIMARY KEY AUTOINCREMENT," +
                "estanquenom STRING NOT NULL," +
                "ubicacion STRING NOT NULL," +
                "tipodepez STRING NOT NULL)");

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_ENCENDERAIREADOR + "("+
                "idaireador INTEGER PRIMARY KEY AUTOINCREMENT," +
                "estado STRING NOT NULL, " +
                "fecha TIMESTAMP NOT NULL,"+
                "idestanque INTEGER NOT NULL)");



    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_TIPOPEZ + TABLE_MEDICIONES + TABLE_ALBERCA + TABLE_ENCENDERAIREADOR);
        onCreate(sqLiteDatabase);

    }
}
