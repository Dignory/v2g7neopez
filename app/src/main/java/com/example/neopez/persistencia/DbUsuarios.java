package com.example.neopez.persistencia;


import android.content.Context;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;


public class DbUsuarios extends NeofishDb{

    Context context;
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = FirebaseFirestore.getInstance();
    public boolean check_usuario = false;
    public boolean check_pass = false;
    public boolean usuario_insertado = false;


    public DbUsuarios(@Nullable Context context) {

        super(context);
        this.context = context;
    }


    public void insertarUsuario(String nomusuario, String contrasena, String correo, OnSuccessListener metodo, OnFailureListener metodomal) {
        Map<String,String> data = new HashMap<String,String>();
        data.put("nomusuario",nomusuario);
        data.put("contrasena",contrasena);
        data.put("correo",correo);
        dbf.collection("usuarios").document().set(data).addOnSuccessListener(metodo).addOnFailureListener(metodomal);
    }

    public void checknomusuario(String nomusuario, OnCompleteListener metodo){

        Query q = dbf.collection("usuarios").whereEqualTo("nomusuario", nomusuario);
        q.get().addOnCompleteListener(metodo);
    }


    public void checkcontrasena(String nomusuario, String contrasena,OnCompleteListener metodo){

        CollectionReference usersRef = dbf.collection("usuarios");

        Query query = dbf.collection("usuarios").whereEqualTo("nomusuario", nomusuario);
        query.get().addOnCompleteListener(metodo);
    }
}


