package com.example.neopez.persistencia;



import android.content.Context;
import android.database.Cursor;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.neopez.domain.Constants;
import com.example.neopez.propiedades.AdaptadorPropiedades;
import com.example.neopez.propiedades.ListPropiedades;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DbEstanque extends NeofishDb{
    Context context;

    AdaptadorPropiedades adaptadorPropiedades;
    RecyclerView recyclerView;
    final String collectionName = Constants.PRODUCTS_COLLECTION_NAME;

    private CollectionReference getCollectionRef(){
        return FirebaseFirestore.getInstance().collection(collectionName);
    }




    public DbEstanque(@Nullable Context context){
        super(context);
        this.context = context;
    }
    public void agregarPropiedades(String idestanque ,String ubicacion,String tipodepez) {
        Map<String, Object> cv = new HashMap<>();
        cv.put("Código Estanque", idestanque );
        cv.put("Ubicacion", ubicacion);
        cv.put("Tipodepez", tipodepez);
        getCollectionRef().add(cv);
    }

    public void eliminarPropiedades(String id) {
        getCollectionRef().document(id).delete();
    }

    public void ActualizarPropiedades(String id,String ubicacion, String tipodepez) {
        Map<String, Object> cv = new HashMap<>();
        cv.put("ubicacion", ubicacion);
        cv.put("tipodepez", tipodepez);
        getCollectionRef().document(id).update(cv);
    }

    public List<ListPropiedades> consultarPropiedades() {
        List<ListPropiedades> listPropiedades = new ArrayList<ListPropiedades>();
        Cursor result = this.getWritableDatabase().query("Alberca", new String[]{"idestanque", "estanquenom", "ubicacion", "tipodepez"}, null, null, null, null, null);

        Query query = getCollectionRef();

        FirestoreRecyclerOptions<ListPropiedades> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<ListPropiedades>()
                .setQuery(query, ListPropiedades.class).build();

        adaptadorPropiedades = new AdaptadorPropiedades(firestoreRecyclerOptions);
        adaptadorPropiedades.notifyDataSetChanged();
        recyclerView.setAdapter(adaptadorPropiedades);

        return listPropiedades;
    }
}


