package com.example.neopez;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.neopez.domain.Constants;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.example.neopez.propiedades.AdaptadorPropiedades;
import com.example.neopez.propiedades.CrearPropiedades;
import com.example.neopez.propiedades.ListPropiedades;
import com.example.neopez.propiedades.PropiedadesAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class VerPropiedades extends Fragment {

    List<ListPropiedades> listPropiedades;
    AdaptadorPropiedades adaptador;
    RecyclerView recycler;
   PropiedadesAdapter propiedadesAdapter;


       public VerPropiedades() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_ver_propiedades, container, false);

            FloatingActionButton btnCrear = root.findViewById(R.id.btnCrear);
            btnCrear.setOnClickListener(view -> ((CardNeopez) getActivity()).switchWindow(new CrearPropiedades()));

            recycler = root.findViewById(R.id.recyclerId);
            recycler.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
            propiedadesAdapter = new PropiedadesAdapter();
            recycler.setAdapter(propiedadesAdapter);

        Query query = FirebaseFirestore.getInstance().collection(Constants.PRODUCTS_COLLECTION_NAME);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (value != null && !value.isEmpty()) {
                    value.getDocuments().forEach(documentSnapshot -> {
                        Log.wtf("doc", documentSnapshot.getId());
                        try {

                            ListPropiedades propiedades = new ListPropiedades();
                            Log.wtf("item", documentSnapshot.get("nombre", String.class));
                            Log.wtf("item", documentSnapshot.get("codigoestanque", String.class));
                            Log.wtf("item", documentSnapshot.get("ubicacion", String.class));
                            Log.wtf("item", documentSnapshot.get("tipo de pez").toString());
                            Log.wtf("item", documentSnapshot.getId());
                        } catch (Exception x) {
                            x.printStackTrace();
                        }
                        propiedadesAdapter.añadirPropiedades(new ListPropiedades());
                    });

                }
            }
        });
        FirestoreRecyclerOptions<ListPropiedades> options = new FirestoreRecyclerOptions.Builder<ListPropiedades>()
                .setQuery(query, ListPropiedades.class)
                .build();


        FloatingActionButton btnSalir = root.findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(view -> {
            ((CardNeopez) getActivity()).finish();
            System.exit(0);
        });

        return root;
    }
}


