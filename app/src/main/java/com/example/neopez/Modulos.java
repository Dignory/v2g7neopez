package com.example.neopez;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class Modulos extends AppCompatActivity {
    List<ListElement> elements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modulos);

        init();

    }
    public void init(){
        elements = new ArrayList<ListElement>();
        elements.add(new ListElement("#775447", "Modulo 2", "Colombia"));
        elements.add(new ListElement("#607d8b", "Modulo 3", "Colombia"));
        elements.add(new ListElement("#03a9f4", "Modulo 4", "Colombia"));
        elements.add(new ListElement("#009688", "Modulo 5", "Colombia"));



        ListAdapter listAdapter = new ListAdapter(elements, this);

        RecyclerView recyclerView = findViewById(R.id.ListRecyclerView);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        recyclerView.setAdapter(listAdapter);

    }
}
